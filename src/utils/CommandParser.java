package utils;

import communication.BookingServer;
import model.*;

import java.io.PrintWriter;

/**
 * Command parser
 * reads the input, parses it and executes the specified command
 */
public class CommandParser {

    private PrintWriter out;
    private User currentUser = null;

    public CommandParser(PrintWriter out) {
        this.out = out;
    }

    public boolean parseAndExecute(String input) {
        if(input.startsWith("login:")) {
            String[] args = input.split(" ");
            String username = args[1];
            String password = args[2];
            System.out.println("trying to login " + username + " with " + password);
            for(User user : BookingServer.manager.allUsers) {
                if(user.getUsername().equals(username)) {
                    if(user.getPassword().equals(password)) {
                        out.println("Signed in as " + user.getType());
                        currentUser = user;
                        return true;
                    } else {
                        out.println("Incorrect password");
                        return true;
                    }
                }
            }
            out.println("User not found !");
            return true;
        }
        if(input.startsWith("signup:")) {
            String[] args = input.split(" ");
            String username = args[1];
            String password = args[2];
            String type = args[3];
            System.out.println("Trying to create account for " + username + ";" + password + ";" + type);
            for(User user : BookingServer.manager.allUsers) {
                if (user.getUsername().equals(username)) {
                    out.println("Username already in use");
                    return true;
                }
            }
            if(type.equalsIgnoreCase("host")) {
                BookingServer.manager.addUser(new Host(username, password));
                out.println("Host account successfully created");
                return true;
            } else {
                if(type.equalsIgnoreCase("guest")) {
                    BookingServer.manager.addUser(new Guest(username, password));
                    out.println("Guest account successfully created");
                    return true;
                } else {
                    out.println("Invalid user type");
                    return true;
                }
            }
        }
        if(input.startsWith("book:")) {
            if(currentUser == null) {
                out.println("You are not logged in, please log in to book!");
                return true;
            }

            String[] args = input.split(" ");
            if(args.length != 3) {
                out.println("Invalid arguments");
                return true;
            }
            String propertyName = args[1];
            String month = args[2];
            Property toBeBooked = null;
            for(Property p : BookingServer.manager.allProperties) {
                if(p.getName().equals(propertyName)) {
                    toBeBooked = p;
                    break;
                }
            }

            if(toBeBooked == null) {
                out.println("The property does not exist");
                return true;
            }
            MonthlyBookingSituation situation = BookingServer.manager.bookingsBrief.get(toBeBooked);
            if(!situation.bookAMonth(month)) {
                out.println("The property is already booked in " + month);
                return true;
            }
            out.println("You booked month " + month + " at " + toBeBooked.getName());
            Booking newBooking = new Booking(toBeBooked, currentUser, situation.getMonthIndex(month) + 1);
            currentUser.addBooking(newBooking);
            BookingServer.manager.addBooking(newBooking);
            return true;
        }
        if(input.startsWith("add_property:")) {
            if(currentUser == null || !currentUser.getType().equals("HOST")) {
                out.println("Sorry, your user cannot add properties");
                return true;
            }
            String[] args = input.split(" ");
            String name = args[1];
            String address = args[2];
            double price = 0;
            try {
                price = Double.parseDouble(args[3]);
            } catch (Exception e) {
                out.println("Invalid price. Try again");
                return true;
            }

            Property newProperty = new Property(name, address, price);
            ((Host)currentUser).addProperty(newProperty);
            BookingServer.manager.addProperty(newProperty);
            BookingServer.manager.bookingsBrief.put(newProperty, new MonthlyBookingSituation());
            out.println("You added a new property: "+ name + " at " + address + " with price = " + price);
            return true;
        }

        if(input.startsWith("list")) {
            String[] args = input.split(" ");
            String tag = null;
            if(args.length == 2) {
                tag = args[1];
            }

            String allPropertiesString = "";
            for(Property p: BookingServer.manager.allProperties) {
                if(tag == null) {
                    allPropertiesString += p.toString();
                } else {
                    if(p.getName().contains(tag)) {
                        allPropertiesString += p.toString();
                    }
                }
            }
            if(allPropertiesString.equals("")) {
                out.println("No properties available");
                return true;
            }
            out.println(allPropertiesString);
            return true;
        }

        if(input.equals("logout")) {
            currentUser = null;
            out.println("You have succesfully logged out");
            return true;
        }
        return false;
    }
}
