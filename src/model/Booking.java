package model;

/**
 * Class for a booking
 */
public class Booking {
    private Property property;
    private User guest;
    private int monthBooked;

    public Booking(Property property, User guest, int monthBooked) {
        this.property = property;
        this.guest = guest;
        this.monthBooked = monthBooked;
    }
}
