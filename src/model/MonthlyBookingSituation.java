package model;

/**
 * Class to keep the track of booked or non-booked months
 */
public class MonthlyBookingSituation {
    private boolean[] monthsBooked;
    private static String[] months = {"January", "February", "March", "April", "May", "June", "July", "August",
            "September", "October", "November", "December"};

    public MonthlyBookingSituation() {
        monthsBooked = new boolean[months.length];
        for(int i=0;i<months.length;i++) {
            monthsBooked[i] = false;
        }
    }

    public int getMonthIndex(String month) {
        for(int i=0;i<months.length;i++) {
            if(month.equals(months[i])) {
                return i;
            }
        }
        return -1;
    }

    public boolean bookAMonth(String whichMonth) {
        int monthNumber = getMonthIndex(whichMonth);
        if(monthNumber == -1 || monthsBooked[monthNumber]) {
            return false;
        }
        monthsBooked[monthNumber] = true;
        return true;
    }
}
