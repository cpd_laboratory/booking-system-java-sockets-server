package model;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * This is the class that keeps all the information for the server
 * it includes all users, all listed properties, all bookings made
 * and a map for each property and its booked/non-booked months
 */
public class BookingManager {
    public List<User> allUsers = new LinkedList<>();
    public List<Property> allProperties = new LinkedList<>();
    public List<Booking> allBookings = new LinkedList<>();
    public Map<Property, MonthlyBookingSituation> bookingsBrief = new HashMap<>();

    public synchronized void addUser(User newUser) {
        allUsers.add(newUser);
    }

    public synchronized void addProperty(Property newProperty) {
        allProperties.add(newProperty);
    }

    public synchronized void addBooking(Booking newBooking) {
        allBookings.add(newBooking);
    }

}
