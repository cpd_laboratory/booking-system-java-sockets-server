package model;

import java.util.LinkedList;
import java.util.List;

public class Host extends User {
    private List<Property> properties;
    public Host(String username, String password) {
        super(username, password, "HOST");
        properties = new LinkedList<>();
    }

    public void addProperty(Property newProperty) {
        properties.add(newProperty);
    }

    public void removeProperty(Property propertyToBeRemoved) {
        properties.remove(propertyToBeRemoved);
    }
}
