package communication;

import model.BookingManager;
import model.User;
import utils.CommandParser;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * Class for server
 */
public class BookingServer {
    private ServerSocket serverSocket;
    public static BookingManager manager = new BookingManager();

    /**
     * Start the server on specified port
     * @param port the port to run server on
     */
    public void start(int port) {
        try {
            serverSocket = new ServerSocket(port);
            while(true) {
                new ClientHandler(serverSocket.accept()).start();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Thread class for handling clients
     */
    private static class ClientHandler extends Thread {
        private Socket clientSocket;
        private PrintWriter out;
        private BufferedReader in;

        public ClientHandler(Socket socket) {
            this.clientSocket = socket;
        }

        @Override
        public void run() {
            //we take the input from the client and parse it to execute a specified command
            try {
                out = new PrintWriter(clientSocket.getOutputStream(), true);
                in = new BufferedReader(
                        new InputStreamReader(clientSocket.getInputStream()));
                CommandParser parser = new CommandParser(out);
                String inputLine;
                while ((inputLine = in.readLine()) != null) {
                    if(!parser.parseAndExecute(inputLine)) {
                        break;
                    }
                }
                in.close();
                out.close();
                clientSocket.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public static void main(String[] args) {
        BookingServer airBnbServer = new BookingServer();
        airBnbServer.start(50);
    }
}
